import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:flutter_map/flutter_map.dart';
import "package:latlong/latlong.dart";

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double latitude;
  double longitude;

  Future<LocationData> locData = Location().getLocation();

  Future<LocationData> getCurrentUserLocation() async {
    final locData = await Location().getLocation();
    setState(() {
      latitude = locData.latitude;
      longitude = locData.longitude;
    });

    print(locData.latitude);
    print(locData.longitude);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Google maps'),
      ),
      body: Column(
        children: [
          longitude == null ? SizedBox() : Container(
            height: 500,
            width: double.infinity,
            child: FlutterMap(
              options: MapOptions(
                center: LatLng(latitude,
                   longitude),
                zoom: 16.0,
              ),
              layers: [
                TileLayerOptions(
                    urlTemplate:
                        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b', 'c']),
                MarkerLayerOptions(
                  markers: [
                    Marker(
                      width: 80.0,
                      height: 80.0,
                      point: LatLng(51.5, -0.09),
                      builder: (ctx) => Container(
                        child: FlutterLogo(),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          ElevatedButton(
            onPressed: getCurrentUserLocation,
            child: Text('open map'),
          ),
        ],
      ),
    );
  }
}

